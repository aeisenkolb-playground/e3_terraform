package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestE3NumericVoodoo(t *testing.T) {
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../",
	})

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	fragment_digits := terraform.OutputList(t, terraformOptions, "fragment_digits")
	expected_fragment_digits := []string{"[192 168 1 5]", "[192 168 1 7]"}
	assert.Equal(t, expected_fragment_digits, fragment_digits)

	fragment_ip := terraform.Output(t, terraformOptions, "fragment_ips")
	assert.Equal(t, "[192.168.1.5 192.168.1.7]", fragment_ip)
}
