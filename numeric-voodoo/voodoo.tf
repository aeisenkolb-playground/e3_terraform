# First time writing Terraform, not sure if it really was intended to
# filter the input without any resources and just using instrinsic functions

output "fragment_digits" {
  value = [for s in local.get_ips_as_list : split(".", s)]
}

output "fragment_ips" {
  value = local.get_ips_as_list
}

locals {
  # Filter fo log entries containing the "Critical" sub-string
  filter_critical = compact([for s in var.log_messages : length(regexall("CRITICAL", s)) > 0 ? s : ""])
  # Getting rid of non-digits
  extract_digits  = [for s in local.filter_critical : replace(split(": ", s)[1], "/[^0-9]/", " ")]
  # Extracting proper ips and removing duplicates
  get_ips_as_list = distinct([for s in local.extract_digits : join(".", compact(split(" ", s)))])
}
