variable "log_messages" {
  type        = list(string)
  description = "Containing the single log entries"
}
